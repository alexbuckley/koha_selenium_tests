package t::lib::Selenium;

use Modern::Perl;

use Koha::Database;

use Carp;
use Module::Load;
use String::Random;
use Time::HiRes qw(gettimeofday);
use Selenium::Waiter qw/wait_until/;
use Try::Tiny;

my $dbh = C4::Context->dbh;
my $login = 'koha';
my $password = 'koha';
my $base_url= 'http://'.C4::Context->preference("staffClientBaseURL")."/cgi-bin/koha/";

sub auth {
    my ( $driver, $login, $password) = @_;
    fill_form( $driver, { userid => 'koha', password => 'koha' } );
    my $login_button = $driver->find_element('//input[@id="submit"]');
    $login_button->submit();
}

sub patron_auth {
    my ( $driver,$patronusername, $patronpassword) = @_;
    fill_form( $driver, { userid => $patronusername, password => $patronpassword } );
    my $login_button = $driver->find_element('//input[@id="submit"]');
    $login_button->submit();
}

sub patron_opac_auth {
    my ( $driver,$patronusername, $patronpassword) = @_;
    fill_form( $driver, { userid => $patronusername, password => $patronpassword } );
    my $login_button = $driver->find_element('//input[@value="Log in"]');
    $login_button->submit();
}

sub fill_form {
    my ( $driver, $values ) = @_;
    while ( my ( $id, $value ) = each %$values ) {
        my $element = $driver->find_element('//*[@id="'.$id.'"]');
        my $tag = $element->get_tag_name();
        if ( $tag eq 'input' ) {
             $driver->find_element('//input[@id="'.$id.'"]')->send_keys($value);
        } elsif ( $tag eq 'select' ) {
             $driver->find_element('//select[@id="'.$id.'"]/option[@value="'.$value.'"]')->click;
        }
    }
}

sub cleanup {
    my ($sample_data, $number_of_biblios_to_insert,$borrowernumber)  = @_;
    my $dbh = C4::Context->dbh;
    $dbh->do(q|DELETE FROM categories WHERE categorycode = ?|, {},     $sample_data->{category}{categorycode});
    $dbh->do(q|DELETE FROM borrowers WHERE userid = ?|, {}, $sample_data->{patron}{userid});
    for my $i ( 1 .. $number_of_biblios_to_insert ) {
        $dbh->do(qq|DELETE FROM biblio WHERE title = "test biblio $i"|);
    };

    $dbh->do(q|DELETE FROM issues where borrowernumber=?|, {}, $borrowernumber);
    $dbh->do(q|DELETE FROM old_issues where borrowernumber=?|, {},$borrowernumber);
    for my $i ( 1 .. $number_of_biblios_to_insert ) {
        $dbh->do(qq|DELETE items, biblio FROM biblio INNER JOIN items ON biblio.biblionumber = items.biblionumber WHERE biblio.title ="test biblio$i"|);
    };
}

sub time_diff {
    my ($prev_time, $lib) = (@_);
    my $now = gettimeofday;
    warn "CP $lib = " . sprintf("%.2f", $now - $prev_time ) . "\n";
   $prev_time = $now;
}

sub pause_driver {
    my ($driver, $element) = @_;
    $driver->set_implicit_wait_timeout(30000);
    my $elem_exists = wait_until {$driver->find_element_by_xpath('$element')};

    if ($elem_exists) {
      $driver->find_element_by_xpath('$element')->click;
    }
}


=head1 NAME

t::lib::Selenium.pm - Koha module for common Selenium functions

=head1 SYNOPSIS


=head1 DESCRIPTION

This Perl module 


This module automatically creates database records for you.
If needed, records for foreign keys are created too.
Values will be randomly generated if not passed to TestBuilder.
Note that you should wrap these actions in a transaction yourself.

=head1 METHODS

=head2 new

    my $builder = t::lib::TestBuilder->new;

    Constructor - Returns the object TestBuilder

=head2 schema

    my $schema = $builder->schema;

    Getter - Returns the schema of DBIx::Class

=head2 delete

    $builder->delete({
        source => $source,
        records => $patron, # OR: records => [ $patron, ... ],
    });

    Delete individual records, created by builder.
    Returns the number of delete attempts, or undef.

=head2 build

    $builder->build({ source  => $source_name, value => $value });

    Create a test record in the table, represented by $source_name.
    The name is required and must conform to the DBIx::Class schema.
    Values may be specified by the optional $value hashref. Will be
    randomized otherwise.
    If needed, TestBuilder creates linked records for foreign keys.
    Returns the values of the new record as a hashref, or undef if
    the record could not be created.

    Note that build also supports recursive hash references inside the
    value hash for foreign key columns, like:
        value => {
            column1 => 'some_value',
            fk_col2 => {
                columnA => 'another_value',
            }
        }
    The hash for fk_col2 here means: create a linked record with build
    where columnA has this value. In case of a composite FK the hashes
    are merged.

    Realize that passing primary key values to build may result in undef
    if a record with that primary key already exists.

=head2 build_object

Given a plural Koha::Object-derived class, it creates a random element, and
returns the corresponding Koha::Object.

    my $patron = $builder->build_object({ class => 'Koha::Patrons' [, value => { ... }] });

=head1 AUTHOR

Alex Buckley <alexbuckley@catalyst.net.nz>

Catalyst Koha Development Team

=head1 COPYRIGHT

Copyright 2017 - Catalyst IT

=head1 LICENSE

This file is part of Koha.

Koha is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

Koha is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Koha; if not, see <http://www.gnu.org/licenses>.

=cut

1;
