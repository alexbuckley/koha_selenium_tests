#!/usr/bin/perl

# This file is part of Koha.
#
# Copyright (C) 2017  Catalyst IT
#
# Koha is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# Koha is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Koha; if not, see <http://www.gnu.org/licenses>.

#This selenium test tests the List module specifcally it tests the ability of a user to create a list and then add items to the list. 

#Note: If you are testing this on kohadevbox with selenium installed in kohadevbox then you need to set the staffClientBaseURL to localhost:8080 and the OPACBaseURL to http://localhost:80

use Modern::Perl;

use Time::HiRes qw(gettimeofday);
use C4::Context;
use C4::Biblio qw( AddBiblio ); # We shouldn't use it

use Test::More tests => 7;
use MARC::Record;
use MARC::Field;
use Selenium::Remote::Driver::Firefox::Profile;

my $dbh = C4::Context->dbh;
my $login = 'koha';
my $password = 'koha';
my $base_url= 'http://'.C4::Context->preference("staffClientBaseURL")."/cgi-bin/koha/";
my $opac_url= C4::Context->preference("OPACBaseURL");

my $number_of_biblios_to_insert = 3;
our $sample_data = {
    category => {
        categorycode    => 'test_cat',
        description     => 'test cat description',
        enrolmentperiod => '12',
        category_type   => 'A'
    },
    patron => {
        surname    => 'test_patron_surname',
        cardnumber => '4242424242',
        userid     => 'test_username',
        password   => 'password',
        password2  => 'password'
    },
};

my $patronusername="test_username";
my $patronpassword="password";

our ( $borrowernumber, $start, $prev_time, $cleanup_needed );

SKIP: {
    eval { require Selenium::Remote::Driver; };
    skip "Selenium::Remote::Driver is needed for selenium tests.", 20 if $@;

    $cleanup_needed = 1;

    open my $fh, '>>', '/tmp/output.txt';

    my $driver = Selenium::Remote::Driver->new;
    my $browser_profile = Selenium::Remote::Driver::Firefox::Profile->new();
    $start = gettimeofday;
    $prev_time = $start;
    $driver->get($base_url."mainpage.pl");
    like( $driver->get_title(), qr(Log in to Koha), );
    auth( $driver, $login, $password );
    time_diff("main");

    #Add biblio
    $borrowernumber = $dbh->selectcol_arrayref(q|SELECT borrowernumber FROM borrowers WHERE userid=?|, {}, $sample_data->{patron}{userid} )->[0];

    my @biblionumbers;
    for my $i ( 1 .. $number_of_biblios_to_insert ) {
       my $biblio = MARC::Record->new();
       my $title = 'test biblio '.$i;
       if ( C4::Context->preference('marcflavour') eq 'UNIMARC' )     {
          $biblio->append_fields(
          MARC::Field->new('200', ' ', ' ', a => 'test biblio     '.$i),
          MARC::Field->new('200', ' ', ' ', f => 'test author     '.$i),
          );
        } else {
          $biblio->append_fields(
          MARC::Field->new('245', ' ', ' ', a => 'test biblio     '.$i),
          MARC::Field->new('100', ' ', ' ', a => 'test author     '.$i),
          );
        }
        my ($biblionumber, $biblioitemnumber) = AddBiblio($biblio,     '');
        push @biblionumbers, $biblionumber;
    }
    time_diff("add biblio");

    #Add items to the biblio
    my $itemtype = $dbh->selectcol_arrayref(q|SELECT itemtype FROM itemtypes|);
    $itemtype = $itemtype->[0];

    for my $biblionumber ( @biblionumbers ) {
        $driver->get($base_url."/cataloguing/additem.pl?biblionumber=$biblionumber");
        like( $driver->get_title(), qr(test biblio \d+ by test author), );
        my $form = $driver->find_element('//form[@name="f"]');
        my $inputs = $driver->find_child_elements($form, '//input[@    type="text"]');
        for my $input ( @$inputs ) {
            next if $input->is_hidden();
            my $id = $input->get_attribute('id');
            next unless $id =~ m|^tag_952_subfield|;
            $input->send_keys('t_value_bib'.$biblionumber);
         }
         $driver->find_element('//input[@name="add_submit"]')->click;
         like( $driver->get_title(), qr($biblionumber.*Items) );

         $dbh->do(q|UPDATE items SET notforloan=0 WHERE biblionumber=?|, {}, $biblionumber );
         $dbh->do(q|UPDATE biblioitems SET itemtype=? WHERE biblionumber=?|, {}, $itemtype, $biblionumber);
         $dbh->do(q|UPDATE items SET itype=? WHERE biblionumber=?|,{}, $itemtype, $biblionumber);
    }
    time_diff("add items");

    $driver->get($base_url."mainpage.pl");
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/div[1]/div[1]/div/ul/li[4]/a')->click;
    $driver->pause(20000);
    $driver->find_element('//a[@id="newshelf"]')->click;
    $driver->pause(20000);
    $driver->find_element('//input[@id="shelfname"]')->send_keys("Test list");
    $driver->find_element('//select[@id="category"]')->click;
    $driver->find_element_by_xpath('/html/body/div[4]/div/div/div/div[2]/div[1]/form/fieldset[1]/ol/li[4]/select/option[2]')->click;
    $driver->find_element_by_xpath('/html/body/div[4]/div/div/div/div[2]/div[1]/form/fieldset[2]/input')->click;
    if ($driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/div[2]/span') ) {
        time_diff("Public list successfully created");
    } else {
        time_diff("Public list not successfully created");
    }

    $driver->get($base_url."mainpage.pl");
    $driver->find_element('//a[@id="ui-id-5"]')->click;
    $driver->find_element('//input[@id="search-form"]')->send_keys("Test biblio");
    $driver->find_element_by_xpath('/html/body/div[2]/div/div[5]/form/input[2]')->click;
    $driver->pause(20000);
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/div[2]/form/table/tbody/tr[2]/td[2]/a')->click;
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/div[4]/div[1]/div[2]/table/tbody/tr/td[1]/input')->click;
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/div[1]/div[4]/button')->click;
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/div[1]/div[4]/ul/li[2]/a')->click;
    my $handles = $driver->get_window_handles;
    $driver->switch_to_window($handles->[1]);
    $driver->pause(20000);
    $driver->find_element_by_xpath('/html/body/div/div/form/fieldset[3]/input[2]')->click;

    $driver->switch_to_window($handles->[0]);
    #Check items added to the list
    $driver->find_element_by_xpath('/html/body/div[1]/div[1]/ul[1]/li[5]/a')->click;
    $driver->find_element_by_xpath('/html/body/div[1]/div[1]/ul[1]/li[5]/ul/li[1]/a')->click;
    $driver->pause(20000);
    $driver->find_element('//a[@id="ui-id-7"]')->click;
    $driver->pause(20000);
    $driver->find_element_by_xpath('//a[text()="Test list"]')->click;
    $driver->pause(20000);
    if ($driver->find_element_by_xpath('/html/body/div[4]/div/div/div/div[2]/form/table')){
        time_diff("List items successfully added");
    } else {
        time_diff("List items not successfully added");
    }

    close $fh;
    $driver->quit();
};

END {
    cleanup() if $cleanup_needed;
};

sub auth {
    my ( $driver, $login, $password) = @_;
    fill_form( $driver, { userid => 'koha', password => 'koha' } );
    my $login_button = $driver->find_element('//input[@id="submit"]');
    $login_button->submit();
}

sub patron_auth {
    my ( $driver,$patronusername, $patronpassword) = @_;
    fill_form( $driver, { userid => $patronusername, password => $patronpassword } );
    my $login_button = $driver->find_element('//input[@id="submit"]');
    $login_button->submit();
}

sub patron_opac_auth {
    my ( $driver,$patronusername, $patronpassword) = @_;
    fill_form( $driver, { userid => $patronusername, password => $patronpassword } );
    my $login_button = $driver->find_element('//input[@value="Log in"]');
    $login_button->submit();
}

sub fill_form {
    my ( $driver, $values ) = @_;
    while ( my ( $id, $value ) = each %$values ) {
        my $element = $driver->find_element('//*[@id="'.$id.'"]');
        my $tag = $element->get_tag_name();
        if ( $tag eq 'input' ) {
            $driver->find_element('//input[@id="'.$id.'"]')->send_keys($value);
        } elsif ( $tag eq 'select' ) {
            $driver->find_element('//select[@id="'.$id.'"]/option[@value="'.$value.'"]')->click;
        }
    }
}

sub cleanup {
    my $dbh = C4::Context->dbh;
    $dbh->do(q|DELETE FROM categories WHERE categorycode = ?|, {}, $sample_data->{category}{categorycode});
    $dbh->do(q|DELETE FROM borrowers WHERE userid = ?|, {}, $sample_data->{patron}{userid});
}

sub time_diff {
    my $lib = shift;
    my $now = gettimeofday;
    warn "CP $lib = " . sprintf("%.2f", $now - $prev_time ) . "\n";
    $prev_time = $now;
}
