#!/usr/bin/perl

# This file is part of Koha.
#
# Koha is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# Koha is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Koha; if not, see <http://www.gnu.org/licenses>.



# wget https://selenium-release.storage.googleapis.com/2.53/selenium-server-standalone-2.53.1.jar # Does not work with 3.4, did not test the ones between
# sudo apt-get install xvfb firefox-esr
# SELENIUM_PATH=/home/vagrant/selenium-server-standalone-2.53.1.jar
# Xvfb :1 -screen 0 1024x768x24 2>&1 >/dev/null &
# DISPLAY=:1 java -jar $SELENIUM_PATH
#

use Modern::Perl;

use Time::HiRes qw(gettimeofday);
use C4::Context;
use C4::Biblio qw( AddBiblio ); # We shouldn't use it

use Test::More tests => 8;
use MARC::Record;
use MARC::Field;

my $dbh = C4::Context->dbh;
my $login = 'koha';
my $password = 'koha';
my $base_url= 'http://'.C4::Context->preference("staffClientBaseURL")."/cgi-bin/koha/";
my $opac_url= C4::Context->preference("OPACBaseURL");

my $number_of_biblios_to_insert = 3;
our $sample_data = {
    category => {
        categorycode    => 'test_cat',
        description     => 'test cat description',
        enrolmentperiod => '12',
        category_type   => 'A'
    },
    patron => {
        surname    => 'test_patron_surname',
        cardnumber => '4242424242',
        userid     => 'test_username',
        password   => 'password',
        password2  => 'password'
    },
};

my $patronusername="test_username";
my $patronpassword="password";

our ( $borrowernumber, $start, $prev_time, $cleanup_needed );

SKIP: {
    eval { require Selenium::Remote::Driver; };
    skip "Selenium::Remote::Driver is needed for selenium tests.", 20 if $@;

    $cleanup_needed = 1;

    open my $fh, '>>', '/tmp/output.txt';

# Go to the mainpage and log in as default user
    my $driver = Selenium::Remote::Driver->new;
    $start = gettimeofday;
    $prev_time = $start;
    $driver->get($base_url."mainpage.pl");
    like( $driver->get_title(), qr(Log in to Koha), );
    auth( $driver, $login, $password );
    time_diff("main");

# Make new biblio
    $borrowernumber = $dbh->selectcol_arrayref(q|SELECT borrowernumber FROM borrowers WHERE userid=?|, {}, $sample_data->{patron}{userid} )->[0];

    my @biblionumbers;
    for my $i ( 1 .. $number_of_biblios_to_insert ) {
        my $biblio = MARC::Record->new();
        my $title = 'test biblio '.$i;
        if ( C4::Context->preference('marcflavour') eq 'UNIMARC' ) {
            $biblio->append_fields(
                MARC::Field->new('200', ' ', ' ', a => 'test biblio '.$i),
                MARC::Field->new('200', ' ', ' ', f => 'test author '.$i),
            );
        } else {
            $biblio->append_fields(
                MARC::Field->new('245', ' ', ' ', a => 'test biblio '.$i),
                MARC::Field->new('100', ' ', ' ', a => 'test author '.$i),
            );
        }
        my ($biblionumber, $biblioitemnumber) = AddBiblio($biblio, '');
        push @biblionumbers, $biblionumber;
    }

    time_diff("add biblio");

# Add items to the biblio
    my $itemtype = $dbh->selectcol_arrayref(q|SELECT itemtype FROM itemtypes|);
    $itemtype = $itemtype->[0];

    for my $biblionumber ( @biblionumbers ) {
        $driver->get($base_url."/cataloguing/additem.pl?biblionumber=$biblionumber");
        like( $driver->get_title(), qr(test biblio \d+ by test author), );
        my $form = $driver->find_element('//form[@name="f"]');
        my $inputs = $driver->find_child_elements($form, '//input[@type="text"]');
        for my $input ( @$inputs ) {
            next if $input->is_hidden();

            my $id = $input->get_attribute('id');
            next unless $id =~ m|^tag_952_subfield|;

            $input->send_keys('t_value_bib'.$biblionumber);
        }

        $driver->find_element('//input[@name="add_submit"]')->click;
        like( $driver->get_title(), qr($biblionumber.*Items) );

        $dbh->do(q|UPDATE items SET notforloan=0 WHERE biblionumber=?|, {}, $biblionumber );
        $dbh->do(q|UPDATE biblioitems SET itemtype=? WHERE biblionumber=?|, {}, $itemtype, $biblionumber);
        $dbh->do(q|UPDATE items SET itype=? WHERE biblionumber=?|, {}, $itemtype, $biblionumber);
    }

    time_diff("add items");

# Load OPAC and create purchase suggestion
   $driver->get($opac_url);
   like( $driver->get_title(), qr(Koha online catalog), );
   warn $driver->get_title();
   $driver->find_element('//a[@id="listsmenu"]')->click;
   $driver->find_element('//a[@href="/cgi-bin/koha/opac-shelves.pl?op=list&category=1"]')->click;
   warn $driver->get_title();
   $driver->find_element_by_xpath('/html/body/div/div[4]/div/div[1]/div[1]/div/div[2]/div/ul/li[7]/a')->click;
   warn $driver->get_title();
   $driver->find_element('//a[@href="/cgi-bin/koha/opac-suggestions.pl?op=add"]')->click;
   $driver->find_element_by_xpath('/html/body/div/div[4]/div/div/div[2]/div/form/fieldset[1]/ol/li[1]/input')->send_keys("The Iliad");
   $driver->find_element_by_xpath('/html/body/div/div[4]/div/div/div[2]/div/form/fieldset[1]/ol/li[2]/input')->send_keys("Homer");
   $driver->find_element_by_xpath('/html/body/div/div[4]/div/div/div[2]/div/form/fieldset[1]/ol/li[4]/input')->send_keys("978-0140275360");
   $driver->find_element_by_xpath('/html/body/div/div[4]/div/div/div[2]/div/form/fieldset[1]/ol/li[5]/input')->send_keys("Pengiun Classics");
   $driver->find_element_by_xpath('/html/body/div/div[4]/div/div/div[2]/div/form/fieldset[1]/ol/li[8]/select')->click;
   $driver->find_element_by_xpath('/html/body/div/div[4]/div/div/div[2]/div/form/fieldset[1]/ol/li[8]/select/option[2]')->click;
   $driver->find_element_by_xpath('/html/body/div/div[4]/div/div/div[2]/div/form/fieldset[1]/ol/li[9]/select')->click;
   $driver->find_element_by_xpath('/html/body/div/div[4]/div/div/div[2]/div/form/fieldset[1]/ol/li[9]/select/option[3]')->click;
   $driver->find_element_by_xpath('/html/body/div/div[4]/div/div/div[2]/div/form/fieldset[2]/input[3]')->click;
   my $submittedstatus = $driver->find_element_by_xpath('/html/body/div/div[4]/div/div/div[2]/div/div')->get_text();
   if ($submittedstatus eq "Your suggestion has been submitted.") {
       time_diff("Purchase suggestion submitted");
   } else {
       time_diff("Purchase suggestion failed");
   }

# Process the purchase suggestion in intranet
   $driver->get($base_url."mainpage.pl");
   $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/div[2]/div/div/a')->click;

    warn $driver->get_title();
    $driver->find_element('//a[@id="CheckAllASKED"]')->click;
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/div[2]/div/form/fieldset[1]/div/div/select[1]')->click;
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/div[2]/div/form/fieldset[1]/div/div/select[1]/option[3]')->click;    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/div[2]/div/form/fieldset[1]/div/div/select[2]')->click;
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/div[2]/div/form/fieldset[1]/div/div/select[2]/option[3]')->click;
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/div[2]/div/form/fieldset[2]/input')->click;
    time_diff("Purchase suggestion approved in Intranet");

# Create vendor
    $driver->find_element_by_xpath('/html/body/div[2]/h1/a')->click;
    warn $driver->get_title();
    $driver->find_element('//a[@class="icon_general icon_acquisitions"]')->click;
    warn $driver->get_title();
    $driver ->get($base_url."acqui/supplier.pl?op=enter");
    warn $driver->get_title();
    $driver->find_element('//input[@id="company"]')->send_keys("Test vendor");
    $driver->find_element('//input[@value="Save"]')->click;

# Create basket
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/div[2]/div/span[2]/a')->click;
    $driver->pause(50000);
    $driver->find_element_by_xpath('/html/body/div[4]/div/div/form/fieldset[1]/ol/li[1]/input')->send_keys("Test basket");
    $driver->find_element('//input[@value="Save"]')->click;

# Add to basket
    $driver->find_element('//a[@href="#addtoBasket"]')->click;
    $driver->find_element('//a[text() = "From a suggestion"]')->click;
    warn $driver->get_title();
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/div/table/tbody/tr/td[4]/a')->click;
    $driver->pause(20000);
    $driver->find_element('//input[@value="Add item"]')->click;
    $driver->find_element('//select[@id="budget_id"]')->click;
    $driver->find_element_by_xpath("/html/body/div[4]/div/div[1]/div/form/fieldset[5]/ol/li[2]/select/option[2]")->click;
    $driver->find_element('//input[@value="Save"]')->click;
    if ($driver->find_element('//div[@id="orders_wrapper"]')) {
        time_diff("Purchase suggestion order placed");
    } else {
        time_diff("Purchase suggestion order not placed");
    }

   close $fh;
   $driver->quit();
};

END {
    cleanup() if $cleanup_needed;
};

sub auth {
    my ( $driver, $login, $password) = @_;
    fill_form( $driver, { userid => 'koha', password => 'koha' } );
    my $login_button = $driver->find_element('//input[@id="submit"]');
    $login_button->submit();
}

sub fill_form {
    my ( $driver, $values ) = @_;
    while ( my ( $id, $value ) = each %$values ) {
        my $element = $driver->find_element('//*[@id="'.$id.'"]');
        my $tag = $element->get_tag_name();
        if ( $tag eq 'input' ) {
            $driver->find_element('//input[@id="'.$id.'"]')->send_keys($value);
        } elsif ( $tag eq 'select' ) {
            $driver->find_element('//select[@id="'.$id.'"]/option[@value="'.$value.'"]')->click;
        }
    }
}

sub cleanup {
    my $dbh = C4::Context->dbh;
    $dbh->do(q|DELETE FROM categories WHERE categorycode = ?|, {}, $sample_data->{category}{categorycode});
    $dbh->do(q|DELETE FROM borrowers WHERE userid = ?|, {}, $sample_data->{patron}{userid});
    for my $i ( 1 .. $number_of_biblios_to_insert ) {
        $dbh->do(qq|DELETE FROM biblio WHERE title = "test biblio $i"|);
    };

    $dbh->do(q|DELETE FROM issues where borrowernumber=?|, {}, $borrowernumber);
    $dbh->do(q|DELETE FROM old_issues where borrowernumber=?|, {}, $borrowernumber);
    for my $i ( 1 .. $number_of_biblios_to_insert ) {
        $dbh->do(qq|DELETE items, biblio FROM biblio INNER JOIN items ON biblio.biblionumber = items.biblionumber WHERE biblio.title = "test biblio$i"|);
    };
}

sub time_diff {
    my $lib = shift;
    my $now = gettimeofday;
    warn "CP $lib = " . sprintf("%.2f", $now - $prev_time ) . "\n";
    $prev_time = $now;
}
