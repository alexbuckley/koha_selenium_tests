#!/usr/bin/perl

# This file is part of Koha.
#
# Koha is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# Koha is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Koha; if not, see <http://www.gnu.org/licenses>.

#This selenium test goes through the cataloging workflow starting on the intranets home page and then using z39.50 targets to add a MARC record. After that items are added to the MARC record

# wget https://selenium-release.storage.googleapis.com/2.53/selenium-server-standalone-2.53.1.jar # Does not work with 3.4, did not test the ones between
# sudo apt-get install xvfb firefox-esr
# SELENIUM_PATH=/home/vagrant/selenium-server-standalone-2.53.1.jar
# Xvfb :1 -screen 0 1024x768x24 2>&1 >/dev/null &
# DISPLAY=:1 java -jar $SELENIUM_PATH
#

use Modern::Perl;

use Time::HiRes qw(gettimeofday);
use C4::Context;
use C4::Biblio qw( AddBiblio ); # We shouldn't use it

use Test::More tests => 1;
use MARC::Record;
use MARC::Field;

my $dbh = C4::Context->dbh;
my $login = 'koha';
my $password = 'koha';
my $base_url= 'http://'.C4::Context->preference("staffClientBaseURL")."/cgi-bin/koha/";
my $opac_url= C4::Context->preference("OPACBaseURL");

my $number_of_biblios_to_insert = 3;
our $sample_data = {
    category => {
        categorycode    => 'test_cat',
        description     => 'test cat description',
        enrolmentperiod => '12',
        category_type   => 'A'
    },
};

my $patronusername="test_username";
my $patronpassword="password";

our ( $borrowernumber, $start, $prev_time, $cleanup_needed );

SKIP: {
    eval { require Selenium::Remote::Driver; };
    skip "Selenium::Remote::Driver is needed for selenium tests.", 20 if $@;

    $cleanup_needed = 1;

    open my $fh, '>>', '/tmp/output.txt';

# Go to the mainpage and log in as default user
    my $driver = Selenium::Remote::Driver->new;
    $start = gettimeofday;
    $prev_time = $start;
    $driver->get($base_url."mainpage.pl");
    like( $driver->get_title(), qr(Log in to Koha), );
    auth( $driver, $login, $password );
    time_diff("main");

# Go to Cataloging page
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/div[1]/div[1]/div/ul/li[5]/a')->click;
    $driver->find_element_by_xpath('/html/body/div[4]/div/div/div[1]/div[2]/button[1]')->click;
    $driver->pause(10000);

#$driver->switch_to_window('/cgi-bin/koha/cataloguing/z3950_search.pl?frameworkcode=');
    my $handles = $driver->get_window_handles;
    $driver->switch_to_window($handles->[1]);
    warn $driver->get_title();
    $driver->find_element_by_xpath('/html/body/div/div/form/div/div[1]/fieldset/ol/li[3]/input')->send_keys("The Iliad");
    $driver->find_element_by_xpath('/html/body/div/div/form/div/div[1]/fieldset/ol/li[4]/input')->send_keys("Homer");
    warn $driver->get_title();
    $driver->find_element_by_xpath('/html/body/div/div/form/div/fieldset/input')->click;
    $driver->pause(20000);

# Import the MARC result
    $driver->find_element_by_xpath('/html/body/div/div/div[1]/table/tbody/tr[1]/td[10]/a')->click;

# Add Koha item type
    $driver->switch_to_window($handles->[0]);
    $driver->pause(20000);

    warn $driver->get_title();
    $driver->find_element_by_xpath('/html/body/div[4]/div/div/div/form/div[3]/div[1]/div[3]/div[2]/input')->click;
    $driver->find_element_by_xpath('/html/body/div[4]/div/div/div/form/div[3]/ul/li[10]/a')->click;
    $driver->find_element_by_xpath('/html/body/div[4]/div/div/div/form/div[3]/div[10]/div[2]/div[3]/div')->click;
    $driver->find_element_by_xpath('/html/body/div[6]/ul/li[1]/div')->click;
    $driver->find_element_by_xpath('/html/body/div[4]/div/div/div/form/div[1]/div[1]/button[2]')->click;
    $driver->find_element_by_xpath('/html/body/div[4]/div/div/div/form/div[1]/div[1]/ul/li[2]/a')->click;

    $driver->find_element_by_xpath('/html/body/div[4]/div/div/div/div/form[2]/button')->click;
# Add item
    warn $driver->get_title();
    $driver->find_element_by_xpath('/html/body/div[4]/div/div/div[2]/div[2]/div/form/fieldset[2]/span[1]/input[1]')->click;
    $driver->find_element('//table[@id="itemst"]');

   close $fh;
   $driver->quit();
};

END {
    cleanup() if $cleanup_needed;
};

sub auth {
    my ( $driver, $login, $password) = @_;
    fill_form( $driver, { userid => 'koha', password => 'koha' } );
    my $login_button = $driver->find_element('//input[@id="submit"]');
    $login_button->submit();
}

sub fill_form {
    my ( $driver, $values ) = @_;
    while ( my ( $id, $value ) = each %$values ) {
        my $element = $driver->find_element('//*[@id="'.$id.'"]');
        my $tag = $element->get_tag_name();
        if ( $tag eq 'input' ) {
            $driver->find_element('//input[@id="'.$id.'"]')->send_keys($value);
        } elsif ( $tag eq 'select' ) {
            $driver->find_element('//select[@id="'.$id.'"]/option[@value="'.$value.'"]')->click;
        }
    }
}

sub cleanup {
    my $dbh = C4::Context->dbh;
    $dbh->do(q|DELETE FROM categories WHERE categorycode = ?|, {}, $sample_data->{category}{categorycode});
    $dbh->do(q|DELETE FROM borrowers WHERE userid = ?|, {}, $sample_data->{patron}{userid});
    for my $i ( 1 .. $number_of_biblios_to_insert ) {
        $dbh->do(qq|DELETE FROM biblio WHERE title = "test biblio $i"|);
    };

    $dbh->do(q|DELETE FROM issues where borrowernumber=?|, {}, $borrowernumber);
    $dbh->do(q|DELETE FROM old_issues where borrowernumber=?|, {}, $borrowernumber);
    for my $i ( 1 .. $number_of_biblios_to_insert ) {
        $dbh->do(qq|DELETE items, biblio FROM biblio INNER JOIN items ON biblio.biblionumber = items.biblionumber WHERE biblio.title = "test biblio$i"|);
    };
}

sub time_diff {
    my $lib = shift;
    my $now = gettimeofday;
    warn "CP $lib = " . sprintf("%.2f", $now - $prev_time ) . "\n";
    $prev_time = $now;
}
