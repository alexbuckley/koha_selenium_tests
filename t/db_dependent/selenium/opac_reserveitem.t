#!/usr/bin/perl

# This file is part of Koha.
#
# Copyright (C) 2017 Catalyst IT 
#
# Koha is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# Koha is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Koha; if not, see <http://www.gnu.org/licenses>.

#This selenium test will test the functionality of placing a reserve, and renewing an item from the OPAC

# Note: If you are testing this on kohadevbox with selenium installed in kohadevbox then you need to set the staffClientBaseURL to localhost:8080 and the OPACBaseURL to localhost:80

use Modern::Perl;

use Time::HiRes qw(gettimeofday);
use C4::Context;
use C4::Biblio qw( AddBiblio ); # We shouldn't use it

use Test::More tests => 3;
use MARC::Record;
use MARC::Field;

my $dbh = C4::Context->dbh;
my $login = 'koha';
my $password = 'koha';
my $base_url= 'http://'.C4::Context->preference("staffClientBaseURL")."/cgi-bin/koha/";
my $opac_url= C4::Context->preference("OPACBaseURL");

my $number_of_biblios_to_insert = 3;
our $sample_data = {
    category => {
        categorycode    => 'test_cat',
        description     => 'test cat description',
        enrolmentperiod => '12',
        category_type   => 'A'
    },
    patron => {
        surname    => 'test_patron_surname',
        cardnumber => '4242424242',
        userid     => 'test_username',
        password   => 'password',
        password2  => 'password'
    },
};

my $patronusername="test_username";
my $patronpassword="password";

our ( $borrowernumber, $start, $prev_time, $cleanup_needed );

SKIP: {
    eval { require Selenium::Remote::Driver; };
    skip "Selenium::Remote::Driver is needed for selenium tests.", 20 if $@;

    $cleanup_needed = 1;

    open my $fh, '>>', '/tmp/output.txt';

# Go to the mainpage and log in as default user
    my $driver = Selenium::Remote::Driver->new;
    $start = gettimeofday;
    $prev_time = $start;
    $driver->get($base_url."mainpage.pl");
    like( $driver->get_title(), qr(Log in to Koha), );
    auth( $driver, $login, $password );
    time_diff("main");

# Go to Cataloging page
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/div[1]/div[1]/div/ul/li[5]/a')->click;
    $driver->find_element_by_xpath('/html/body/div[4]/div/div/div[1]/div[2]/button[1]')->click;
    $driver->pause(10000);

    my $handles = $driver->get_window_handles;
    $driver->switch_to_window($handles->[1]);
    warn $driver->get_title();
    $driver->find_element_by_xpath('/html/body/div/div/form/div/div[1]/fieldset/ol/li[3]/input')->send_keys("Wind in the Willows");
    warn $driver->get_title();
    $driver->find_element_by_xpath('/html/body/div/div/form/div/fieldset/input')->click;
    $driver->pause(20000);

# Import the MARC result
    $driver->find_element_by_xpath('/html/body/div/div/div[1]/table/tbody/tr[1]/td[10]/a')->click;

# Add Koha item type
    $driver->switch_to_window($handles->[0]);
    $driver->pause(20000);

    warn $driver->get_title();
    $driver->find_element_by_xpath('/html/body/div[4]/div/div/div/form/div[3]/div[1]/div[3]/div[2]/input')->click;
    $driver->find_element_by_xpath('/html/body/div[4]/div/div/div/form/div[3]/ul/li[10]/a')->click;
    $driver->find_element_by_xpath('/html/body/div[4]/div/div/div/form/div[3]/div[10]/div[2]/div[3]/div')->click;
    $driver->find_element_by_xpath('/html/body/div[6]/ul/li[1]/div')->click;
    $driver->find_element_by_xpath('/html/body/div[4]/div/div/div/form/div[1]/div[1]/button[2]')->click;
    $driver->find_element_by_xpath('/html/body/div[4]/div/div/div/form/div[1]/div[1]/ul/li[2]/a')->click;


# Add item
    warn $driver->get_title();
    $driver->find_element_by_xpath('/html/body/div[4]/div/div/div[2]/div[2]/div/form/fieldset[1]/ol/li[23]/div/input[1]')->send_keys("TEST12345");
    $driver->find_element_by_xpath('/html/body/div[4]/div/div/div[2]/div[2]/div/form/fieldset[2]/span[1]/input[1]')->click;
    $driver->find_element('//table[@id="itemst"]');

# Load OPAC
   $driver->get($opac_url);
   like( $driver->get_title(), qr(Koha online catalog), );
   $driver->find_element_by_xpath('/html/body/div/div[2]/div/div/div/div[1]/form/div/input')->send_keys('Wind in the willows');
   $driver->find_element_by_xpath('/html/body/div/div[2]/div/div/div/div[1]/form/div/button')->click;
   $driver->pause(20000);
   $driver->find_element_by_xpath('/html/body/div/div[4]/div/div[1]/div[2]/div/ul/li[1]/a')->click;
   $driver->pause(20000);
   $driver->find_element('//input[@value="Confirm hold"]')->click;
   if ($driver->find_element_by_xpath('//table[@id="holdst"]') ){
       time_diff("Hold successfully placed");
   } else {
       time_diff("Hold not successfully placed");
   }

#Check the item out in the intranet
   $driver->get($base_url."mainpage.pl");
   $driver->find_element_by_xpath('/html/body/div[2]/div/div[1]/form/div/input[1]')->send_keys('koha');
   $driver->find_element_by_xpath('/html/body/div[2]/div/div[1]/form/div/input[2]')->click;

#select the patron and checkout the item
   $driver->find_element_by_xpath('/html/body/div[4]/div[1]/div/div/fieldset/div/table/tbody/tr[2]/td[1]/a')->click;
   warn $driver->get_title();
   $driver->find_element('//input[@id="barcode"]')->send_keys("TEST12345");
   $driver->find_element_by_xpath('/html/body/div[4]/div[1]/div[1]/div/div[3]/div[1]/form/fieldset/button')->click;

#Renew the item in the OPAC
   $driver->get($opac_url);
   like( $driver->get_title(), qr(Koha online catalog), );
   warn $driver->get_title();
   $driver->find_element_by_xpath('/html/body/div[1]/div[1]/div/div/div/ul/li[3]/a')->click;
   $driver->find_element_by_xpath('/html/body/div[1]/div[1]/div/div/div/ul/li[3]/ul/li[3]/a')->click;
   $driver->find_element_by_xpath('/html/body/div/div[4]/div/div/div[1]/div/div[2]/div/ul/li[1]/a')->click;
   warn $driver->get_title();
   $driver->find_element_by_xpath('/html/body/div/div[4]/div/div/div[2]/div/div[2]/div[2]/form[2]/input[4]')->click;
   if ($driver->find_element_by_xpath('/html/body/div/div[4]/div/div/div[2]/div/div[2]/div[2]/form[1]/div/table/tbody/tr/td[6]/span[1]')) {
       time_diff("Item has been renewed");
   } else {
       time_diff("Item has not been successfully renewed");
   }

   close $fh;
   $driver->quit();
};

END {
    cleanup() if $cleanup_needed;
};

sub auth {
    my ( $driver, $login, $password) = @_;
    fill_form( $driver, { userid => 'koha', password => 'koha' } );
    my $login_button = $driver->find_element('//input[@id="submit"]');
    $login_button->submit();
}

sub fill_form {
    my ( $driver, $values ) = @_;
    while ( my ( $id, $value ) = each %$values ) {
        my $element = $driver->find_element('//*[@id="'.$id.'"]');
        my $tag = $element->get_tag_name();
        if ( $tag eq 'input' ) {
            $driver->find_element('//input[@id="'.$id.'"]')->send_keys($value);
        } elsif ( $tag eq 'select' ) {
            $driver->find_element('//select[@id="'.$id.'"]/option[@value="'.$value.'"]')->click;
        }
    }
}

sub cleanup {
    my $dbh = C4::Context->dbh;
    $dbh->do(q|DELETE FROM categories WHERE categorycode = ?|, {}, $sample_data->{category}{categorycode});
    $dbh->do(q|DELETE FROM borrowers WHERE userid = ?|, {}, $sample_data->{patron}{userid});
    for my $i ( 1 .. $number_of_biblios_to_insert ) {
        $dbh->do(qq|DELETE FROM biblio WHERE title = "test biblio $i"|);
    };

    $dbh->do(q|DELETE FROM issues where borrowernumber=?|, {}, $borrowernumber);
    $dbh->do(q|DELETE FROM old_issues where borrowernumber=?|, {}, $borrowernumber);
    for my $i ( 1 .. $number_of_biblios_to_insert ) {
        $dbh->do(qq|DELETE items, biblio FROM biblio INNER JOIN items ON biblio.biblionumber = items.biblionumber WHERE biblio.title = "test biblio$i"|);
    };
}

sub time_diff {
    my $lib = shift;
    my $now = gettimeofday;
    warn "CP $lib = " . sprintf("%.2f", $now - $prev_time ) . "\n";
    $prev_time = $now;
}
