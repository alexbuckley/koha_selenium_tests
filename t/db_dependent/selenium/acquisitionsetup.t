#!/usr/bin/perl

# This file is part of Koha.
#
# Koha is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# Koha is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Koha; if not, see <http://www.gnu.org/licenses>.



# wget https://selenium-release.storage.googleapis.com/2.53/selenium-server-standalone-2.53.1.jar # Does not work with 3.4, did not test the ones between
# sudo apt-get install xvfb firefox-esr
# SELENIUM_PATH=/home/vagrant/selenium-server-standalone-2.53.1.jar
# Xvfb :1 -screen 0 1024x768x24 2>&1 >/dev/null &
# DISPLAY=:1 java -jar $SELENIUM_PATH
#
# Remove the rentalcharge:
# % UPDATE itemtypes SET rentalcharge = 0;
#
# Then you can execute the test file.
#
# If you get:
# Wide character in print at /usr/local/share/perl/5.20.2/Test2/Formatter/TAP.pm line 105.
# #                   'Koha › Patrons › Add patron test_patron_surname (Adult)'
# #     doesn't match '(?^u:Patron details for test_patron_surname)'
#
# Ignore and retry (FIXME LATER...)

use Modern::Perl;

use Time::HiRes qw(gettimeofday);
use C4::Context;
use C4::Biblio qw( AddBiblio ); # We shouldn't use it

use Test::More tests => 1;
use MARC::Record;
use MARC::Field;

my $dbh = C4::Context->dbh;
my $login = 'koha';
my $password = 'koha';
my $base_url= 'http://'.C4::Context->preference("staffClientBaseURL")."/cgi-bin/koha/";
my $opac_url= C4::Context->preference("OPACBaseURL");

my $number_of_biblios_to_insert = 3;
our $sample_data = {
    category => {
        categorycode    => 'test_cat',
        description     => 'test cat description',
        enrolmentperiod => '12',
        category_type   => 'A'
    },
    patron => {
        surname    => 'test_patron_surname',
        cardnumber => '4242424242',
        userid     => 'test_username',
        password   => 'password',
        password2  => 'password'
    },
};

my $patronusername="test_username";
my $patronpassword="password";

our ( $borrowernumber, $start, $prev_time, $cleanup_needed );

SKIP: {
    eval { require Selenium::Remote::Driver; };
    skip "Selenium::Remote::Driver is needed for selenium tests.", 20 if $@;

    $cleanup_needed = 1;

    open my $fh, '>>', '/tmp/output.txt';

# Go to the mainpage and log in as default user
    my $driver = Selenium::Remote::Driver->new;
    $start = gettimeofday;
    $prev_time = $start;
    $driver->get($base_url."mainpage.pl");
    like( $driver->get_title(), qr(Log in to Koha), );
    auth( $driver, $login, $password );
    time_diff("main");

#Create a currency
    $driver->get($base_url.'admin/currency.pl?op=add_form');
    $driver->find_element('//input[@id="currency_code"]')->send_keys("USD");
    $driver->find_element('//input[@id="rate"]')->send_keys("1");
    $driver->find_element('//input[@id="symbol"]')->send_keys('$');
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/form/fieldset[1]/ol/li[6]/input')->click;
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/form/fieldset[2]/input')->click;
    time_diff("Added currency");

#Create budget
    $driver->get($base_url.'admin/aqbudgetperiods.pl?op=add_form');
    $driver->find_element('//input[@id="from"]')->send_keys("07/25/2017");
    $driver->find_element('//input[@id="budget_period_total"]')->clear();
    $driver->find_element('//input[@id="budget_period_total"]')->send_keys(100);
    $driver->find_element('//input[@id="to"]')->send_keys("07/30/2017");
    $driver->find_element('//input[@id="budget_period_description"]')->send_keys("Test budget2");
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/form/fieldset[1]/ol/li[5]/input')->click;
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/form/fieldset[2]/input')->click;
    time_diff("Added budget");

#Create fund 

    $driver->get($base_url.'admin/aqbudgets.pl?op=add_form&budget_period_id=1');
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/form/fieldset[1]/ol/li[1]/input')->send_keys("Test");
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/form/fieldset[1]/ol/li[2]/input')->send_keys("Test fund");
    $driver->find_element('//input[@id="budget_amount"]')->clear();
    $driver->find_element('//input[@id="budget_amount"]')->send_keys("50");
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/form/fieldset[2]/input[1]')->click;
    time_diff("Added fund");

    close $fh;
    $driver->quit();
};

END {
    cleanup() if $cleanup_needed;
};

sub auth {
    my ( $driver, $login, $password) = @_;
    fill_form( $driver, { userid => 'koha', password => 'koha' } );
    my $login_button = $driver->find_element('//input[@id="submit"]');
    $login_button->submit();
}

sub fill_form {
    my ( $driver, $values ) = @_;
    while ( my ( $id, $value ) = each %$values ) {
        my $element = $driver->find_element('//*[@id="'.$id.'"]');
        my $tag = $element->get_tag_name();
        if ( $tag eq 'input' ) {
            $driver->find_element('//input[@id="'.$id.'"]')->send_keys($value);
        } elsif ( $tag eq 'select' ) {
            $driver->find_element('//select[@id="'.$id.'"]/option[@value="'.$value.'"]')->click;
        }
    }
}

sub cleanup {
    my $dbh = C4::Context->dbh;
    $dbh->do(q|DELETE FROM categories WHERE categorycode = ?|, {}, $sample_data->{category}{categorycode});
    $dbh->do(q|DELETE FROM borrowers WHERE userid = ?|, {}, $sample_data->{patron}{userid});
    for my $i ( 1 .. $number_of_biblios_to_insert ) {
        $dbh->do(qq|DELETE FROM biblio WHERE title = "test biblio $i"|);
    };

    $dbh->do(q|DELETE FROM issues where borrowernumber=?|, {}, $borrowernumber);
    $dbh->do(q|DELETE FROM old_issues where borrowernumber=?|, {}, $borrowernumber);
    for my $i ( 1 .. $number_of_biblios_to_insert ) {
        $dbh->do(qq|DELETE items, biblio FROM biblio INNER JOIN items ON biblio.biblionumber = items.biblionumber WHERE biblio.title = "test biblio$i"|);
    };
}

sub time_diff {
    my $lib = shift;
    my $now = gettimeofday;
    warn "CP $lib = " . sprintf("%.2f", $now - $prev_time ) . "\n";
    $prev_time = $now;
}
