#!/usr/bin/perl

# This file is part of Koha.
#
# Copyright (C) 2017  Catalyst IT
#
# Koha is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# Koha is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Koha; if not, see <http://www.gnu.org/licenses>.

#This selenium test is to test the Koha web installer and onboarding tool. Before running the test make sure you have restarted memcached and have dropped and recreated the Koha database. 
#Note: When running the test database errors will be displayed however just ignore those errors and the test output will be displayed below it
# wget https://selenium-release.storage.googleapis.com/2.53/selenium-server-standalone-2.53.1.jar # Does not work with 3.4, did not test the ones between
# sudo apt-get install xvfb firefox-esr
# SELENIUM_PATH=/home/vagrant/selenium-server-standalone-2.53.1.jar
# Xvfb :1 -screen 0 1024x768x24 2>&1 >/dev/null &
# DISPLAY=:1 java -jar $SELENIUM_PATH
#
# Remove the rentalcharge:
# % UPDATE itemtypes SET rentalcharge = 0;
#
# Then you can execute the test file.
#
# If you get:
# Wide character in print at /usr/local/share/perl/5.20.2/Test2/Formatter/TAP.pm line 105.
# #                   'Koha › Patrons › Add patron test_patron_surname (Adult)'
# #     doesn't match '(?^u:Patron details for test_patron_surname)'
#
# Ignore and retry (FIXME LATER...)

#Note: If you are testing this on kohadevbox with selenium installed in kohadevbox then you need to set the staffClientBaseURL to localhost:8080 and the OPACBaseURL to http://localhost:80

use Modern::Perl;

use Time::HiRes qw(gettimeofday);
use C4::Context;
use C4::Biblio qw( AddBiblio ); # We shouldn't use it

use Test::More tests => 1;
use MARC::Record;
use MARC::Field;

my $dbh = C4::Context->dbh;
my $login = 'koha_kohadev';
my $password = 'password';

my $base_url= 'http://'.C4::Context->preference("staffClientBaseURL")."/cgi-bin/koha/";
my $opac_url= C4::Context->preference("OPACBaseURL");

our $sample_data = {
    category => {
        categorycode    => 'test_cat',
        description     => 'test cat description',
        enrolmentperiod => '12',
        category_type   => 'A'
    },
    patron => {
        surname    => 'test_patron_surname',
        cardnumber => '4242424242',
        userid     => 'test_username',
        password   => 'password',
        password2  => 'password'
    },
};

my $patronusername="test_username";
my $patronpassword="password";

our ( $borrowernumber, $start, $prev_time, $cleanup_needed );

SKIP: {
    eval { require Selenium::Remote::Driver; };
    skip "Selenium::Remote::Driver is needed for selenium tests.", 20 if $@;

    $cleanup_needed = 1;

    open my $fh, '>>', '/tmp/output.txt';
#   my $output = ` read -p 'What is your staff intranet base URL? ' baseURL
#   echo
#   echo Thankyou for entering \$baseURL?`;

    my $driver = Selenium::Remote::Driver->new;
    $start = gettimeofday;
    $prev_time = $start;
    $driver->get("http://localhost:8080/cgi-bin/koha/mainpage.pl");
    auth( $driver, $login, $password );
    time_diff("main");

    #Select a language
    $driver->find_element_by_xpath('/html/body/div/div/div/form/div/select')->click();
    $driver->find_element_by_xpath('/html/body/div/div/div/form/div/select/option[5]')->click();
    $driver->find_element_by_xpath('/html/body/div/div/div/form/p/input')->click();
    time_diff("Selected english language");

    $driver->maximize_window();
    $driver->pause(50000);
    warn $driver->get_title();
#Check perl dependencies
    $driver->find_element_by_xpath('/html/body/div/div/div/form/p/input')->click();
    time_diff("Continued through checking perl dependencies");

#Check database settings
    $driver->find_element_by_xpath('/html/body/div/div/div/form/p/input')->click();
    $driver->find_element_by_xpath('/html/body/div/div/div/form/p/input')->click();
    time_diff("Continued through checking database settings");

#Setup database
    $driver->find_element_by_xpath('/html/body/div/div/div/form/p/input')->click();
    $driver->find_element_by_xpath('/html/body/div/div/div/form/p/input')->click();
    time_diff("Database set up");

#Install basic configs
    $driver->find_element_by_xpath('/html/body/div/div/div/p[2]/a')->click();
    time_diff("Basic configs installed");

    $driver->maximize_window();
    $driver->pause(50000);
#Select MARC flavour
    warn $driver->get_title();
    $driver->find_element('//input[@value="Continue to the next step"]')->click;
    time_diff("MARC21 installed");

#Install default settings 
    $driver->find_element_by_xpath('/html/body/div/div/div/form/p[2]/input')->click();
    $driver->find_element_by_xpath('/html/body/div/div/div/form/p[2]/input')->click();
    time_diff("Default settings installed");

#Start onboarding tool
    $driver->find_element_by_xpath('/html/body/div/div/div/p/a')->click();
    time_diff("Start onboarding tool");

#Onboarding tool create a library
    $driver->pause(50000);
    warn $driver->get_title();
    $driver->find_element_by_xpath('/html/body/div/div/div/form/fieldset/ol/li[1]/input')->send_keys("Test");
    $driver->find_element_by_xpath('/html/body/div/div/div/form/fieldset/ol/li[2]/input')->send_keys("Test library");
    $driver->find_element_by_xpath('/html/body/div/div/div/form/fieldset/input[3]')->click();
    time_diff("Library created");

#Onboarding tool create a patron category
    $driver->find_element_by_xpath('/html/body/div[1]/div/div/form/fieldset/ol/li[1]/input')->send_keys("PAT");
    $driver->find_element_by_xpath('/html/body/div[1]/div/div/form/fieldset/ol/li[2]/input')->send_keys("Test patron category");
    $driver->find_element_by_xpath('/html/body/div[1]/div/div/form/fieldset/ol/li[6]/fieldset/ol/li[3]/input')->send_keys('07/29/2020');
    $driver->find_element_by_xpath('/html/body/div[1]/div/div/form/fieldset/input[3]')->click;
    time_diff("Patron category created");

    $driver->set_window_size(1000, 1000);
    $driver->maximize_window();
#Onboarding tool Create Koha adminstrator patron
    $driver->pause(50000);
    warn $driver->get_title();
    $driver->find_element_by_xpath('/html/body/div/div/div/form/fieldset/ol[1]/li[1]/input')->send_keys("Smith");
    $driver->find_element_by_xpath('/html/body/div/div/div/form/fieldset/ol[1]/li[2]/input')->send_keys("Mary");

    $driver->pause(50000);
    warn $driver->get_title();
    $driver->find_element_by_xpath('/html/body/div/div/div/form/fieldset/ol[2]/li[1]/input')->send_keys("10203");
    $driver->find_element_by_xpath('/html/body/div/div/div/form/fieldset/ol[4]/li[1]/input')->send_keys("Mary");
    $driver->find_element_by_xpath('/html/body/div/div/div/form/fieldset/ol[4]/li[2]/input')->send_keys("password");
    $driver->find_element_by_xpath('/html/body/div/div/div/form/fieldset/ol[4]/li[3]/input')->send_keys("password");
    $driver->find_element_by_xpath('/html/body/div/div/div/form/input')->click();
    time_diff("Create administrator patron");

#Onboarding tool Create item type
    $driver->find_element_by_xpath('/html/body/div/div/div/form/fieldset/ol/li[1]/input')->send_keys("BOOK");
    $driver->find_element_by_xpath('/html/body/div/div/div/form/fieldset/ol/li[2]/input')->send_keys("Non-electronic book");
    $driver->find_element_by_xpath('/html/body/div/div/div/form/fieldset/input[3]')->click();
    time_diff("Item type created");

#Onboarding to Create circulation rule
    $driver->find_element_by_xpath('/html/body/div/div/div/form/input')->click();
    $driver->find_element_by_xpath('/html/body/div/div/div/a')->click();
    time_diff("Circulation rule created");

#Login to Koha
    $driver->find_element_by_xpath('/html/body/div/div/div/form/p[1]/input')->send_keys("Mary");
    $driver->find_element_by_xpath('/html/body/div/div/div/form/p[2]/input')->send_keys("password");
    $driver->find_element_by_xpath('/html/body/div/div/div/form/p[4]/input')->click();
    like( $driver->get_title(), qr(Koha staff client), );

    close $fh;
    $driver->quit();
};

END {
    cleanup() if $cleanup_needed;
};

sub auth {
    my ( $driver, $login, $password) = @_;
    fill_form( $driver, { userid => 'koha_kohadev', password => 'password' } );
    my $login_button = $driver->find_element_by_xpath('/html/body/div/div/div/form/fieldset/input');
    $login_button->submit();
}

sub patron_auth {
    my ( $driver,$patronusername, $patronpassword) = @_;
    fill_form( $driver, { userid => $patronusername, password => $patronpassword } );
    my $login_button = $driver->find_element('//input[@id="submit"]');
    $login_button->submit();
}

sub patron_opac_auth {
    my ( $driver,$patronusername, $patronpassword) = @_;
    fill_form( $driver, { userid => $patronusername, password => $patronpassword } );
    my $login_button = $driver->find_element('//input[@value="Log in"]');
    $login_button->submit();
}

sub fill_form {
    my ( $driver, $values ) = @_;
    while ( my ( $id, $value ) = each %$values ) {
        my $element = $driver->find_element('//*[@id="'.$id.'"]');
        my $tag = $element->get_tag_name();
        if ( $tag eq 'input' ) {
            $driver->find_element('//input[@id="'.$id.'"]')->send_keys($value);
        } elsif ( $tag eq 'select' ) {
            $driver->find_element('//select[@id="'.$id.'"]/option[@value="'.$value.'"]')->click;
        }
    }
}

sub cleanup {
    my $dbh = C4::Context->dbh;
    $dbh->do(q|DELETE FROM categories WHERE categorycode = ?|, {}, $sample_data->{category}{categorycode});
    $dbh->do(q|DELETE FROM borrowers WHERE userid = ?|, {}, $sample_data->{patron}{userid});
}

sub time_diff {
    my $lib = shift;
    my $now = gettimeofday;
    warn "CP $lib = " . sprintf("%.2f", $now - $prev_time ) . "\n";
    $prev_time = $now;
}
