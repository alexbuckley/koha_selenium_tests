#!/usr/bin/perl

# This file is part of Koha.
#
# Copyright (C) 2017  Catalyst IT
#
# Koha is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# Koha is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Koha; if not, see <http://www.gnu.org/licenses>.

#This selenium test tests the Koha Administration module functionality including adding circ rules, item types and modifying frameworks

#Note: If you are testing this on kohadevbox with selenium installed in kohadevbox then you need to set the staffClientBaseURL to localhost:8080 and the OPACBaseURL to http://localhost:80

use Modern::Perl;

use Time::HiRes qw(gettimeofday);
use C4::Context;
use C4::Biblio qw( AddBiblio ); # We shouldn't use it

use Test::More tests => 1;
use MARC::Record;
use MARC::Field;
use t::lib::Selenium;

my $dbh = C4::Context->dbh;
my $login = 'koha';
my $password = 'koha';
my $base_url= 'http://'.C4::Context->preference("staffClientBaseURL")."/cgi-bin/koha/";
my $opac_url= C4::Context->preference("OPACBaseURL");

my $number_of_biblios_to_insert = 0;
our $sample_data = {
    category => {
        categorycode    => 'test_cat',
        description     => 'test cat description',
        enrolmentperiod => '12',
        category_type   => 'A'
    },
    patron => {
        surname    => 'test_patron_surname',
        cardnumber => '4242424242',
        userid     => 'test_username',
        password   => 'password',
        password2  => 'password'
    },
};

my $patronusername="test_username";
my $patronpassword="password";

our ( $borrowernumber, $start, $prev_time, $cleanup_needed );

SKIP: {
    eval { require Selenium::Remote::Driver; };
    skip "Selenium::Remote::Driver is needed for selenium tests.", 20 if $@;

    $cleanup_needed = 1;

    open my $fh, '>>', '/tmp/output.txt';

    my $driver = Selenium::Remote::Driver->new;
    $start = gettimeofday;
    $prev_time = $start;
    $driver->get($base_url."mainpage.pl");
    like( $driver->get_title(), qr(Log in to Koha), );
    t::lib::Selenium::auth( $driver, $login, $password );
    t::lib::Selenium::time_diff($prev_time, "main");

    # Navigate to the Administration area and create an item type
    $driver->find_element('//a[@href="/cgi-bin/koha/admin/admin-home.pl"]')->click;
#    t::lib::Selenium::pause_driver($driver, '/html/body/div[4]/div/div/div[2]/div[1]/dl[1]/dt[2]/a');
#   $driver->pause(20000);
    $driver->pause(20000);
#    $driver->find_element_by_xpath('/html/body/div[4]/div/div/div[2]/div[1]/dl[1]/dt[2]/a')->click;
#   $$driver->find_element_by_xpath('/html/body/div[4]/div/div/div[2]/div[1]/dl[1]/dt[2]/a')->click;
    $driver->pause(20000);
    $driver->find_element('//a[@id="newitemtype"]')->click;
    $driver->pause(20000);
    $driver->find_element('//input[@id="itemtype"]')->send_keys("DVD");
    $driver->find_element('//input[@id="description"]')->send_keys("Digital Optical Disc");
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/form/fieldset[2]/input')->click;
    if ( $driver->find_element('//a[@href="/cgi-bin/koha/admin/itemtypes.pl?op=add_form&itemtype=DVD"]')) {
        t::lib::Selenium::time_diff($prev_time, "Item type successfully added");
    } else {
        t::lib::Selenium::time_diff($prev_time, "Item type not added successfully");
    }

    # Add circulation/fine rules
    $driver->pause(20000);
    $driver->find_element('//input[@href="/cgi-bin/koha/admin/admin-home.pl"]')->click;
    $driver->pause(20000);
    $driver->find_element_by_xpath('/html/body/div[4]/div/div/div[2]/div[1]/dl[2]/dt[2]/a')->click;
    $driver->pause(20000);
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/div[2]/form[3]/table/tbody/tr[2]/td[3]/input')->send_keys("5");
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/div[2]/form[3]/table/tbody/tr[2]/td[4]/input')->send_keys("4");
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/div[2]/form[3]/table/tbody/tr[2]/td[16]/input')->send_keys("4");
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/div[2]/form[3]/table/tbody/tr[2]/td[17]/input')->send_keys("10");
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/div[2]/form[3]/table/tbody/tr[2]/td[22]/input')->send_keys("4");
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/div[2]/form[3]/table/tbody/tr[2]/td[23]/input')->send_keys("4");
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/div[2]/form[3]/table/tbody/tr[2]/td[28]/button[1]')->click;
    t::lib::Selenium::time_diff($prev_time, "Circulation rule added");

    # Modify frameworks
    $driver->pause(20000);
    $driver->find_element_by_xpath('/html/body/div[3]/a[2]')->click;
    $driver->pause(20000);
    $driver->find_element_by_xpath('/html/body/div[4]/div/div/div[2]/div[2]/dl[1]/dt[1]/a')->click;
    $driver->pause(20000);
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/div[2]/table/tbody/tr[5]/td[3]/div/a')->click;
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/div[2]/table/tbody/tr[5]/td[3]/div/ul/li[1]/a')->click;
    $driver->pause(20000);
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/div[2]/table/tbody/tr[3]/td[6]/div/a')->click;
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/div[2]/table/tbody/tr[3]/td[6]/div/ul/li[3]/a')->click;
    $driver->pause(20000);
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/div/form[1]/button')->click;
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/div/form/button')->click;
    $driver->pause(20000);
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/div[1]/a')->click;
    $driver->pause(20000);
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/form/fieldset[1]/ol/li[1]/input')->send_keys("092");
   $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/form/fieldset[1]/ol/li[2]/input')->send_keys("Local Dewey");
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/form/fieldset[1]/ol/li[3]/input')->send_keys("Local Dewey");
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/form/fieldset[2]/input')->click;
    $driver->pause(20000);
    t::lib::Selenium::time_diff($prev_time, "Added subfield to framework");

    #Add library
    $driver->get_title();
    $driver->find_element_by_xpath('/html/body/div[2]/h1/a')->click;
    $driver->pause(20000);
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/div[1]/div[2]/div/ul/li[5]/a')->click;
    $driver->pause(20000);
    $driver->find_element_by_xpath('/html/body/div[4]/div/div/div[2]/div[1]/dl[1]/dt[1]/a')->click;
    $driver->pause(20000);
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/div[1]/a[1]')->click;
    $driver->pause(20000);
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/form/fieldset[1]/ol/li[1]/input')->send_keys("Test");
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/form/fieldset[1]/ol/li[2]/input')->send_keys("Test library");
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/form/fieldset[3]/input')->click;
    $driver->pause(20000);
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/div[3]/div[1]/div[2]/label/select')->click;
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/div[3]/div[1]/div[2]/label/select/option[3]')->click;
    if ($driver->find_element('//td[text()="Test library"]')) {
        t::lib::Selenium::time_diff($prev_time, "Library successfully added");
    } else {
        t::lib::Selenium::time_diff($prev_time, "Library not successfully added");
    }


#Add authorised values
    $driver->find_element_by_xpath('/html/body/div[3]/a[2]')->click;
    $driver->pause(20000);
    $driver->find_element_by_xpath('/html/body/div[4]/div/div/div[2]/div[1]/dl[1]/dt[3]/a')->click;
    $driver->pause(20000);
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/form/select')->click;
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/form/select/option[7]')->click;
    $driver->pause(20000);
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/div[1]/a[1]')->click;
    $driver->pause(20000);
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/form/fieldset[1]/ol/li[2]/input')->send_keys("HBK");
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/form/fieldset[1]/ol/li[3]/input')->send_keys("Hardback");
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/form/fieldset[1]/ol/li[4]/input')->send_keys("Hardcover");
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/form/fieldset[2]/input[2]')->click;
    if ($driver->find_element('//td[text()="HBK"]')) {
        t::lib::Selenium::time_diff($prev_time, "Authorised value successfully added");
    } else {
        t::lib::Seleniumtime_diff($prev_time, "Authorised value not successfully added");
    }

#Patron category
    $driver->find_element_by_xpath('/html/body/div[3]/a[2]')->click;
    $driver->pause(20000);
    $driver->find_element_by_xpath('/html/body/div[4]/div/div/div[2]/div[1]/dl[2]/dt[1]/a')->click;
    $driver->pause(20000);
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/div[1]/a')->click;
    $driver->pause(20000);
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/form/fieldset[1]/ol/li[1]/input')->send_keys("AD");
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/form/fieldset[1]/ol/li[2]/input')->send_keys("Adult");
    $driver->find_element_by_xpath("/html/body/div[4]/div/div[1]/div/form/fieldset[1]/ol/li[3]/fieldset/ol/li[1]/input")->send_keys(12);
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/form/fieldset[1]/ol/li[10]/select')->click;
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/form/fieldset[1]/ol/li[10]/select/option[6]')->click;
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/form/fieldset[2]/input')->click;
    $driver->pause(20000);
    if ($driver->find_element('//td[text()="AD"]')) {
        t::lib::Selenium::time_diff($prev_time, "Patron category successfully added");
    } else {
        t::lib::Selenium::time_diff(i$prev_time, "Patron category not successfully added");
    }

#Add patron attribute type
    $driver->find_element_by_xpath('/html/body/div[3]/a[2]')->click;
    $driver->pause(20000);
    $driver->find_element_by_xpath('/html/body/div[4]/div/div/div[2]/div[1]/dl[2]/dt[3]/a')->click;
    $driver->pause(20000);
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/div[2]/a')->click;
    $driver->pause(20000);
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/form/fieldset[1]/ol/li[1]/input')->send_keys("DL");
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/form/fieldset[1]/ol/li[2]/input')->send_keys("Driver's License");
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/form/fieldset[1]/ol/li[11]/select')->click;
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/form/fieldset[1]/ol/li[11]/select/option[2]')->click;
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/form/fieldset[2]/input')->click;
    $driver->pause(20000);
    if ($driver->find_element('//td[text()="DL"]')) {
        t::lib::Selenium::time_diff($prev_time, "Patron attribute type successfully added");
    } else {
        t::lib::Selenium::time_diff($prev_time, "Patron attribute type not successfully added");
    }

#Add Z39.50 target server
    $driver->find_element_by_xpath('/html/body/div[3]/a[2]')->click;
    $driver->pause(20000);
    $driver->find_element_by_xpath('/html/body/div[4]/div/div/div[2]/div[2]/dl[3]/dt[1]/a')->click;
    $driver->pause(20000);
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/div[1]/a[1]')->click;
    $driver->pause(20000);
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/form/fieldset[1]/ol/li[1]/input')->send_keys("University of London");
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/form/fieldset[1]/ol/li[2]/input')->send_keys("consull.ull.ac.uk");
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/form/fieldset[1]/ol/li[3]/input')->send_keys(210);
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/form/fieldset[1]/ol/li[4]/input')->send_keys("INNOPAC");
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/form/fieldset[1]/ol/li[9]/select')->click;
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/form/fieldset[1]/ol/li[9]/select/option[4]')->click;
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/form/fieldset[2]/input')->click;
    $driver->pause(20000);
    if ($driver->find_element('//td[text()="consull.ull.ac.uk:210"]')) {
        t::lib::Selenium::time_diff($prev_time, "Z39.50 target successfully added");
    } else {
        t::lib::Selenium::time_diff($prev_time, "Z39.50 target not successfully added");
    }

#Add item circulation alerts
    $driver->find_element_by_xpath('/html/body/div[3]/a[2]')->click;
    $driver->pause(20000);
    $driver->find_element_by_xpath('/html/body/div[4]/div/div/div[2]/div[1]/dl[2]/dt[6]/a')->click;
    $driver->pause(20000);
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/div[2]/div[1]/table/tbody/tr[1]/td[1]')->click;
    t::lib::Selenium::time_diff($prev_time, "Added item circulation alert");

#Add city
    $driver->find_element_by_xpath('/html/body/div[3]/a[2]')->click;
    $driver->pause(20000);
    $driver->find_element_by_xpath('/html/body/div[4]/div/div/div[2]/div[1]/dl[2]/dt[7]/a')->click;
    $driver->pause(20000);
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/div[1]/a')->click;
    $driver->pause(20000);
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/form/fieldset[1]/ol/li[1]/input')->send_keys("Wellington");
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/form/fieldset[1]/ol/li[3]/input')->send_keys(6011);
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/form/fieldset[1]/ol/li[4]/input')->send_keys("New Zealand");
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/form/fieldset[2]/input')->click;
    $driver->pause(20000);
    if ($driver->find_element('//td[text()="Wellington"]')) {
        t::lib::Selenium::time_diff($prev_time, "City successfully added");
    } else {
        t::lib::Selenium::time_diff($prev_time, "City not successfully added");
    }

    close $fh;
    $driver->quit();
};

END {
    t::lib::Selenium::cleanup($sample_data,$number_of_biblios_to_insert,$borrowernumber) if $cleanup_needed;
};

