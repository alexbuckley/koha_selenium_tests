#!/usr/bin/perl

# This file is part of Koha.
#
# Copyright (C) 2017  Catalyst IT
#
# Koha is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# Koha is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Koha; if not, see <http://www.gnu.org/licenses>.

#This selenium test tests the following functionality of the Tools module:
#* Create patorn list
#* Create patron club template
#* Create patron club
#* Create notice
#* Create overdue notice and status trigger
#* Create patron card batch
#* Create patron card text layout
#* Create patron card template
#* Set holiday with calendar
#* Create CSV profile 
#* View logs
#* Create news item
#* Create new quote

#Note: If you are testing this on kohadevbox with selenium installed in kohadevbox then you need to set the staffClientBaseURL to localhost:8080 and the OPACBaseURL to http://localhost:80

use Modern::Perl;

use Time::HiRes qw(gettimeofday);
use C4::Context;
use C4::Biblio qw( AddBiblio ); # We shouldn't use it

use Test::More tests => 1;
use MARC::Record;
use MARC::Field;

my $dbh = C4::Context->dbh;
my $login = 'koha';
my $password = 'koha';
my $base_url= 'http://'.C4::Context->preference("staffClientBaseURL")."/cgi-bin/koha/";
my $opac_url= C4::Context->preference("OPACBaseURL");

our $sample_data = {
    category => {
        categorycode    => 'test_cat',
        description     => 'test cat description',
        enrolmentperiod => '12',
        category_type   => 'A'
    },
    patron => {
        surname    => 'test_patron_surname',
        cardnumber => '4242424242',
        userid     => 'test_username',
        password   => 'password',
        password2  => 'password'
    },
};

my $patronusername="test_username";
my $patronpassword="password";

our ( $borrowernumber, $start, $prev_time, $cleanup_needed );

SKIP: {
    eval { require Selenium::Remote::Driver; };
    skip "Selenium::Remote::Driver is needed for selenium tests.", 20 if $@;

    $cleanup_needed = 1;

    open my $fh, '>>', '/tmp/output.txt';

    my $driver = Selenium::Remote::Driver->new;
    $start = gettimeofday;
    $prev_time = $start;
    $driver->get($base_url."mainpage.pl");
    like( $driver->get_title(), qr(Log in to Koha), );
    auth( $driver, $login, $password );
    time_diff("main");

#Navigate into the tools module
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/div[1]/div[2]/div/ul/li[4]/a')->click;

#Create a patron list
    $driver->pause(20000);
    $driver->find_element_by_xpath('/html/body/div[4]/div/div/div[1]/dl/dt[1]/a')->click;
    $driver->pause(20000);
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/div[1]/div/a')->click;
    $driver->pause(20000);
    $driver->find_element_by_xpath('/html/body/div[4]/div/form/fieldset[1]/ol/li[1]/input')->send_keys("Test list");
    $driver->find_element_by_xpath('/html/body/div[4]/div/form/fieldset[2]/input[2]')->click;
    $driver->pause(20000);
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/form[1]/fieldset/input')->send_keys("koha");
    $driver->pause(20000);
    $driver->find_element_by_xpath('/html/body/ul[2]/li/a')->click;
    $driver->pause(20000);
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/form[1]/fieldset/fieldset/fieldset/a')->click;
    $driver->pause(20000);
    if ($driver->find_element('//td[text()="Test list"]')) {
        time_diff("Patron list successfully created and added patron to it");
    } else {
        time_diff("Patron list not successfully created");
    }

#Create patron club template
    $driver->find_element_by_xpath('/html/body/div[3]/a[2]')->click;
    $driver->pause(20000);
    $driver->find_element_by_xpath('/html/body/div[4]/div/div/div[1]/dl/dt[2]/a')->click;
    $driver->pause(20000);
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/div[1]/div/a')->click;
    $driver->pause(20000);
    $driver->find_element_by_xpath('/html/body/div[4]/div[1]/div/form/fieldset[1]/ol/li[1]/input')->send_keys("Test patron club template");
    $driver->find_element_by_xpath('/html/body/div[4]/div[1]/div/form/fieldset[1]/ol/li[2]/input')->send_keys("This is the test template");
    $driver->find_element_by_xpath('/html/body/div[4]/div[1]/div/form/fieldset[1]/ol/li[3]/input')->click;
    $driver->find_element_by_xpath('/html/body/div[4]/div[1]/div/form/fieldset[2]/fieldset/a')->click;
    $driver->pause(2000);
    $driver->find_element_by_xpath('/html/body/div[4]/div[1]/div/form/fieldset[2]/div/div/ol/li[1]/input')->send_keys("Test field");
    $driver->find_element_by_xpath('/html/body/div[4]/div[1]/div/form/fieldset[4]/input[2]')->click;
    $driver->pause(2000);
    if ($driver->find_element('//td[text()="Test patron club template"]')) {
        time_diff("Test template successfully created");
    } else {
        time_diff("Test template not successfully created");
    }

#Create patron club
    $driver->pause(20000);
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/div[4]/div/button')->click;
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/div[4]/div/ul/li[1]/a')->click;
    $driver->pause(20000);
    $driver->find_element_by_xpath('/html/body/div[4]/div/div/form/fieldset[1]/ol/li[1]/input')->send_keys("Centerville patron club");
    $driver->find_element_by_xpath('/html/body/div[4]/div/div/form/fieldset[1]/ol/li[2]/input')->send_keys("Patron club for all patrons of Centerville library");
    $driver->find_element_by_xpath('/html/body/div[4]/div/div/form/fieldset[1]/ol/li[5]/select')->click;
    $driver->find_element('//option[@value="CPL"]')->click;
    $driver->find_element_by_xpath('/html/body/div[4]/div/div/form/fieldset[2]/input')->click;
    $driver->pause(2000);
    if ($driver->find_element('//td[text()="Centerville patron club"]')) {
        time_diff("Patron club successfully created");
    } else {
        time_diff("Patron club not successfully created");
    }

#Create notice
    $driver->find_element_by_xpath('/html/body/div[3]/a[2]')->click;
    $driver->find_element_by_xpath('/html/body/div[4]/div/div/div[1]/dl/dt[5]/a')->click;
    $driver->pause(2000);
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/form/div/button')->click;
    $driver->pause(20000);
    $driver->find_element_by_xpath('/html/body/div[4]/div/div/div/form/fieldset/ol/li[3]/input')->send_keys("TEST_NOTICE");
    $driver->find_element_by_xpath('/html/body/div[4]/div/div/div/form/fieldset/ol/li[4]/input')->send_keys("Test notice");
    $driver->find_element_by_xpath('/html/body/div[4]/div/div/div/form/div/div/h3[1]')->click;
    $driver->find_element_by_xpath('/html/body/div[4]/div/div/div/form/div/div/fieldset[1]/ol/li[2]/input')->send_keys("Test title");
    $driver->find_element_by_xpath('/html/body/div[4]/div/div/div/form/div/div/fieldset[1]/ol/li[3]/table/tbody/tr/td[1]/select/option[2]')->click;
    $driver->find_element_by_xpath('/html/body/div[4]/div/div/div/form/div/div/fieldset[1]/ol/li[3]/table/tbody/tr/td[2]/button')->click;
    $driver->find_element_by_xpath('/html/body/div[4]/div/div/div/div[1]/div/button[1]')->click;
    $driver->pause(20000);
    if ($driver->find_element('//td[text()="TEST_NOTICE"]')) {
        time_diff("Notice successfully created");
    } else {
        time_diff("Notice not successfully created");
    }

    #Create overdue notice and status trigger
    $driver->find_element_by_xpath('/html/body/div[3]/a[2]')->click;
    $driver->find_element_by_xpath('/html/body/div[4]/div/div/div[1]/dl/dt[6]/a')->click;
    $driver->pause(20000);
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/form[2]/div/div[1]/table/tbody/tr[1]/td[1]/input')->send_keys(12);
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/form[2]/div/div[1]/table/tbody/tr[1]/td[2]/select')->click;
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/form[2]/div/div[1]/table/tbody/tr[1]/td[2]/select/option[2]')->click;
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/form[2]/fieldset/input')->click;
    $driver->pause(20000);
    my $overdue_action = $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/form[2]/div[2]/div[1]/table/tbody/tr[1]/td[1]/input');
    if ($overdue_action->get_attribute('value') == 12) {
        time_diff("Overdue action successfully added");
    } else {
        time_diff("Overdue action not successfully added");
    }

# Patron card creator
# Create patron card batch
    $driver->find_element_by_xpath('/html/body/div[3]/a[2]')->click;
    $driver->pause(20000);
    $driver->find_element_by_xpath('/html/body/div[4]/div/div/div[1]/dl/dt[7]/a')->click;
    $driver->pause(20000);
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/div/div[1]/div[1]/button')->click;
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/div/div[1]/div[1]/ul/li[1]/a')->click;
    $driver->pause(20000);
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/div[2]/form/div/fieldset/ol/li/textarea')->send_keys(1);
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/div[2]/div[2]/a[1]/i')->click;
    $driver->pause(20000);
    if ($driver->find_element('//td[text()=1]')) {
        time_diff("Patron batch successfully added");
    } else {
        time_diff("Patron batch not successfully added");
    }

# Create patron card text layout
    $driver->get($base_url."/cgi-bin/koha/tools/tools-home.pl");
    $driver->find_element_by_xpath('/html/body/div[4]/div/div/div[1]/dl/dt[7]/a')->click;
    $driver->pause(2000);
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/div/div[1]/div[1]/button')->click;
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/div/div[1]/div[1]/ul/li[3]/a')->click;
    $driver->pause(20000);
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/form/div/div[1]/fieldset/ol/li[1]/fieldset/ol/li[1]/input')->send_keys("Test patron card text layout");
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/form/div/div[2]/fieldset/ol/li[1]/fieldset/ol/li/input')->click;
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/form/fieldset/input[1]')->click;
    $driver->pause(20000);
    if ($driver->find_element('//td[text()="Test patron card text layout"]')) {
        time_diff("Patron card text layout successfully created");
    } else {
        time_diff("Patron card text layout not successfully created");
    }

#Create patron card template
    $driver->find_element_by_xpath('/html/body/div[3]/a[3]')->click;
    $driver->pause(20000);
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/div/div[1]/div[1]/button')->click;
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/div/div[1]/div[1]/ul/li[4]/a')->click;
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/form/div[1]/div[1]/fieldset/ol/li[2]/input')->clear();
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/form/div[1]/div[1]/fieldset/ol/li[2]/input')->send_keys("Test patron card template");
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/form/div[1]/div[1]/fieldset/ol/li[3]/textarea')->clear();
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/form/div[1]/div[1]/fieldset/ol/li[3]/textarea')->send_keys("This is a test patron card template");
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/form/div[2]/fieldset/input[1]')->click;
    $driver->pause(20000);
    if ($driver->find_element('//td[text()="Test patron card template"]')) {
        time_diff("Patron card template successfully created");
    } else {
        time_diff("Patron card template not successfully created");
    }

#Set holidays with calendar
    $driver->find_element_by_xpath('/html/body/div[3]/a[2]')->click;
    $driver->pause(20000);
    $driver->find_element_by_xpath('/html/body/div[4]/div/div/div[2]/dl/dt[1]/a')->click;
    $driver->pause(2000);
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/div/div[1]/div[3]/div/table/tbody/tr[2]/td[2]/a')->click;
    $driver->find_element_by_xpath('/html/body/div[5]/div[2]/button[1]')->click;
    $driver->find_element_by_xpath('/html/body/div[5]/table/tbody/tr[2]/td[3]/a')->click;
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/div/div[1]/div[2]/form/fieldset/ol/li[4]/input')->send_keys("Test holiday");
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/div/div[1]/div[2]/form/fieldset/fieldset/input')->click;
    $driver->pause(2000);
    if ($driver->find_element('//td[text()="Test holiday"]')) {
        time_diff("Unique holiday successfully created");
    } else {
        time_diff("Unique holiday not successfully created");
    }

#Create CSV profile
    $driver->find_element_by_xpath('/html/body/div[3]/a[2]')->click;
    $driver->pause(2000);
    $driver->find_element_by_xpath('/html/body/div[3]/a[2]')->click;
    $driver->pause(2000);
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/div/a')->click;
    $driver->pause(2000);
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/form/fieldset[1]/ol/li[1]/input')->send_keys("Test CSV profile");
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/form/fieldset[1]/ol/li[10]/textarea')->send_keys("Personal name=200|Entry element=210$a|300|009");
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/form/fieldset[2]/input')->click;
    $driver->pause(20000);
    if ($driver->find_element('//td[text()="Test CSV profile"]')) {
        time_diff("CSV profile successfully added");
    } else {
        time_diff("CSV profile not successfully added");
    }

#View logs
    $driver->find_element_by_xpath('/html/body/div[3]/a[2]')->click;
    $driver->pause(20000);
    $driver->find_element_by_xpath('/html/body/div[4]/div/div/div[2]/dl/dt[3]/a')->click;
    $driver->pause(2000);
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/form/fieldset[3]/input[1]')->click;
    $driver->pause(3000);
    if ($driver->find_element('//th[text()="Date"]')) {
        time_diff("Logs successfully displayed");
    } else {
        time_diff("Logs not successfully displayed");
    }

#Create news item
    $driver->find_element_by_xpath('/html/body/div[3]/a[2]')->click;
    $driver->pause(2000);
    $driver->find_element_by_xpath('/html/body/div[4]/div/div/div[2]/dl/dt[4]/a')->click;
    $driver->pause(2000);
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/div[1]/a')->click;
    $driver->pause(2000);
    $driver->find_element_by_xpath('/html/body/div[4]/div/div/div/form/fieldset[1]/ol/li[3]/input')->send_keys("Test news item");
    $driver->find_element_by_xpath('/html/body/div[4]/div/div/div/form/fieldset[1]/ol/li[4]/input')->click;
    $driver->find_element_by_xpath('/html/body/div[5]/table/tbody/tr[2]/td[2]/a')->click;
    $driver->find_element_by_xpath('/html/body/div[4]/div/div/div/form/fieldset[2]/input')->click;
    $driver->pause(2000);
    if ($driver->find_element('//td[text()="Test news item"]')) {
        time_diff("News item successfully created");
    } else {
        time_diff("News item not successfully created");
    }

#Add new quote
    $driver->find_element_by_xpath('/html/body/div[3]/a[2]')->click;
    $driver->pause(2000);
    $driver->find_element_by_xpath('/html/body/div[4]/div/div/div[2]/dl/dt[6]/a')->click;
    $driver->pause(2000);
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/div[1]/div[1]/a')->click;
    $driver->pause(2000);
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/div[3]/table/tbody/tr[2]/td[2]/input')->send_keys("Albert Einstein");
    $driver->send_modifier('Enter', 'down');
    $driver->find_element_by_xpath('/html/body/div[4]/div/div[1]/div/div[3]/table/tbody/tr[2]/td[3]/input')->send_keys("Stay away from negative people. They have a problem for every solution");
    $driver->send_modifier('Enter', 'down');
    $driver->pause(2000);
    if ($driver->find_element('//td[text()="Albert Einstein"]')) {
        time_diff("New quote successfully added");
    } else {
        time_diff("New quote not successfully added");
    }


    close $fh;
    $driver->quit();
};

END {
    cleanup() if $cleanup_needed;
};

sub auth {
    my ( $driver, $login, $password) = @_;
    fill_form( $driver, { userid => 'koha', password => 'koha' } );
    my $login_button = $driver->find_element('//input[@id="submit"]');
    $login_button->submit();
}

sub patron_auth {
    my ( $driver,$patronusername, $patronpassword) = @_;
    fill_form( $driver, { userid => $patronusername, password => $patronpassword } );
    my $login_button = $driver->find_element('//input[@id="submit"]');
    $login_button->submit();
}

sub patron_opac_auth {
    my ( $driver,$patronusername, $patronpassword) = @_;
    fill_form( $driver, { userid => $patronusername, password => $patronpassword } );
    my $login_button = $driver->find_element('//input[@value="Log in"]');
    $login_button->submit();
}

sub fill_form {
    my ( $driver, $values ) = @_;
    while ( my ( $id, $value ) = each %$values ) {
        my $element = $driver->find_element('//*[@id="'.$id.'"]');
        my $tag = $element->get_tag_name();
        if ( $tag eq 'input' ) {
            $driver->find_element('//input[@id="'.$id.'"]')->send_keys($value);
        } elsif ( $tag eq 'select' ) {
            $driver->find_element('//select[@id="'.$id.'"]/option[@value="'.$value.'"]')->click;
        }
    }
}

sub cleanup {
    my $dbh = C4::Context->dbh;
    $dbh->do(q|DELETE FROM categories WHERE categorycode = ?|, {}, $sample_data->{category}{categorycode});
    $dbh->do(q|DELETE FROM borrowers WHERE userid = ?|, {}, $sample_data->{patron}{userid});
}

sub time_diff {
    my $lib = shift;
    my $now = gettimeofday;
    warn "CP $lib = " . sprintf("%.2f", $now - $prev_time ) . "\n";
    $prev_time = $now;
}
